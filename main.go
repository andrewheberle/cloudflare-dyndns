package main

import (
	"context"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"strings"

	cloudflare "github.com/cloudflare/cloudflare-go"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

func getCurrentIP() (string, error) {

	// Returns current IP information
	var ip string

	url := "http://checkip.amazonaws.com/"
	// Build new HTTP request
	req, err := http.NewRequest(http.MethodGet, url, nil)

	if err != nil {
		return ip, fmt.Errorf("%v", err)
	}

	// Create new HTTP client
	client := &http.Client{}
	// Perform request
	res, err := client.Do(req)
	if err != nil {
		// Problem with request
		return ip, fmt.Errorf("%v", err)
	}
	// Make sure to close response body once finished
	defer res.Body.Close()

	// Check HTTP response
	if res.StatusCode != http.StatusOK {
		// Didn't get a 200 response so return error
		return ip, fmt.Errorf("HTTP status not OK: %v", res.Status)
	}

	// Read response body
	b, err := ioutil.ReadAll(res.Body)

	if err != nil {
		return ip, fmt.Errorf("%v", err)
	}

	// Trim newline from end of response and Parse as IP address
	trimmed := strings.TrimSuffix(string(b), "\n")
	address := net.ParseIP(trimmed)

	// Check that returned address was valid
	if address == nil {
		return ip, fmt.Errorf("Invalid IP address: %v", trimmed)
	}

	ip = address.String()

	// return New IP info
	return ip, nil
}

func main() {

	var recordID, recordIP string
	var api *cloudflare.API
	var err error

	// Command line flags
	flag.Int("ttl", 600, "TTL of DNS record (overidden with \"CF_TTL\" environment variable)")
	flag.String("type", "A", "Type of DNS record (overidden with \"CF_TYPE\" environment variable)")
	flag.String("name", "", "Name of DNS record (overidden with \"CF_NAME\" environment variable)")
	flag.String("zone", "", "Name of DNS zone (overidden with \"CF_ZONE\" environment variable)")
	flag.Bool("quiet", false, "No output except for errors")
	flag.Bool("dryrun", false, "Do not make any changes")
	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)
	pflag.Parse()
	viper.BindPFlags(pflag.CommandLine)

	// env var handling
	viper.SetEnvPrefix("cf")
	viper.AutomaticEnv()

	// Check for required values
	if !viper.IsSet("name") {
		log.Fatalf("Name not set\n")
	}
	if !viper.IsSet("zone") {
		log.Fatalf("Zone not set\n")
	}

	// get values from viper
	zoneName := viper.GetString("zone")
	recordName := viper.GetString("name")
	recordType := viper.GetString("type")
	quiet := viper.GetBool("quiet")
	dryRun := viper.GetBool("dryrun")
	recordTTL := viper.GetInt("ttl")

	// create new api object (attempt via token first then try with key and email)
	if viper.IsSet("api_token") {
		api, err = cloudflare.NewWithAPIToken(viper.GetString("api_token"))
		if err != nil {
			log.Fatalf("Problem constucting API object: %v\n", err)
		}
	} else if viper.IsSet("api_key") && viper.IsSet("api_email") {
		api, err = cloudflare.New(viper.GetString("api_key"), viper.GetString("api_email"))
		if err != nil {
			log.Fatalf("Problem constucting API object: %v\n", err)
		}
	} else {
		log.Fatalf("Problem constucting API object: no credentials provided\n")
	}

	// get ZoneID
	if !quiet {
		log.Printf("Retrieving Zone ID for \"%v\"...\n", zoneName)
	}
	zoneID, err := api.ZoneIDByName(zoneName)
	if err != nil {
		log.Fatalf("Problem retrieving Zone ID for \"%v\": %v\n", zoneName, err)
	}
	if !quiet {
		log.Printf("Zone ID for \"%v\": %v\n", zoneName, zoneID)
	}

	// Get DNS Record
	if !quiet {
		log.Printf("Retrieving DNS records for \"%v\"...\n", recordName+"."+zoneName)
	}
	recordFilter := cloudflare.DNSRecord{Name: recordName + "." + zoneName, Type: recordType}
	recs, err := api.DNSRecords(context.Background(), zoneID, recordFilter)
	if err != nil {
		log.Fatalf("Problem retrieving DNS records for \"%v\": %v\n", recordName+"."+zoneName, err)
	}

	// Get Current Address
	currentIP, err := getCurrentIP()
	if err != nil {
		log.Fatalf("Error retrieving current IP address: %v\n", err)
	}
	if !quiet {
		log.Printf("Current discovered IP Address: %v\n", currentIP)
	}

	switch len(recs) {
	case 0:
		// No record found so we create one
		if !quiet {
			log.Printf("Current record not found in the DNS.")
		}

		// Create new RR object
		newRecord := cloudflare.DNSRecord{Name: recordName + "." + zoneName, Type: recordType, Content: currentIP, TTL: recordTTL}
		if !quiet {
			log.Printf("Creating DNS record to \"%v\"\n", currentIP)
		}
		if !dryRun {
			if _, err := api.CreateDNSRecord(context.Background(), zoneID, newRecord); err != nil {
				log.Fatalf("Problem creating DNS record: %v\n", err)
			}
		} else {
			if !quiet {
				log.Printf("DNS record creation skipped.\n")
			}
		}
		if !quiet {
			log.Printf("DNS record created successfully.\n")
		}
	case 1:
		// Single record found so we update it
		recordIP = recs[0].Content
		recordID = recs[0].ID
		if !quiet {
			log.Printf("Current record in the DNS is: %v\n", recordIP)
		}
		// Update DNS record if old and new IP address dont match
		if recordIP != currentIP {
			// Create new RR object
			newRecord := cloudflare.DNSRecord{Name: recordName + "." + zoneName, Type: recordType, Content: currentIP, TTL: recordTTL}
			if !quiet {
				log.Printf("Updating DNS record from \"%v\" to \"%v\"\n", currentIP, recordIP)
			}
			if !dryRun {
				if api.UpdateDNSRecord(context.Background(), zoneID, recordID, newRecord) != nil {
					log.Fatalf("Problem updating DNS record: %v\n", err)
				}
			} else {
				if !quiet {
					log.Printf("DNS record update skipped.\n")
				}
			}
			if !quiet {
				log.Printf("DNS record update succeeded.\n")
			}
		} else {
			if !quiet {
				log.Printf("No DNS record update required.\n")
			}
		}
	default:
		// More than one found (should not happen) so we bail out
		log.Fatalf("Multiple DNS records were retrieved\n")
	}
}
