.PHONY: all windows linux clean

all: windows linux

linux: cloudflare-dyndns

windows: cloudflare-dyndns.exe

cloudflare-dyndns: main.go go.mod
	GOOS=linux CGO_ENABLED=0 go build -o $@

cloudflare-dyndns.exe: main.go go.mod
	GOOS=windows CGO_ENABLED=0 go build -o $@

clean:
	-@$(RM) cloudflare-dyndns
	-@$(RM) cloudflare-dyndns.exe
