# cloudflare-dyndns [![build status](https://gitlab.com/andrewheberle/cloudflare-dyndns/badges/master/build.svg)](https://gitlab.com/andrewheberle/cloudflare-dyndns/commits/master) [![Go Report Card](https://goreportcard.com/badge/gitlab.com/andrewheberle/cloudflare-dyndns)](https://goreportcard.com/report/gitlab.com/andrewheberle/cloudflare-dyndns)

## Description

Performs a "dynamic" DNS update on a DNS record within CloudFlare DNS.

Looks up current IP address, current address in DNS and updates if required.

## Command Line

```sh
export CF_TOKEN=cloudflareapitoken
./cloudflare-dyndns --name blah --zone example.com
```

## Docker

```sh
docker run -d -e CF_TOKEN=cloudflareapitoken -e CF_NAME=blah -e CF_ZONE=example.com registry.gitlab.com/andrewheberle/cloudflare-dyndns:latest
```

## License

The MIT License (MIT)

Copyright (c) 2021 Andrew Heberle
