module gitlab.com/andrewheberle/cloudflare-dyndns

go 1.16

require (
	github.com/cloudflare/cloudflare-go v0.14.0
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.7.1
	golang.org/x/net v0.0.0-20210316092652-d523dce5a7f4 // indirect
	golang.org/x/text v0.3.5 // indirect
	golang.org/x/time v0.0.0-20210220033141-f8bda1e9f3ba // indirect
)
