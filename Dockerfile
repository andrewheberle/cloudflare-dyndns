FROM golang:1.16 AS builder

ENV CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64

WORKDIR /build

COPY . .

RUN make cloudflare-dyndns

FROM gcr.io/distroless/base-debian10:nonroot

COPY --from=builder /build/cloudflare-dyndns /usr/local/bin/

ENTRYPOINT [ "cloudflare-dyndns" ]
